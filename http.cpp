#include "http.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QEventLoop>
#include <QStringList>
#include <QThread>

http::http(QObject *parent) : QObject(parent)
{
    na_manager = new QNetworkAccessManager();
}

void http::doQuer()
{
    QString req = "http://mobdevtestpoly.pythonanywhere.com/exam?code_word=6";
    request.setUrl(req);
    QEventLoop loop;
    connect(na_manager, SIGNAL(finished(QNetworkReply*)), &loop, SLOT(quit()));

    reply = na_manager->get(request);

    loop.exec();
    qDebug() << "Отправка запроса на сервер http://mobdevtestpoly.pythonanywhere.com/exam?code_word=6";
    qDebug() << "Ответ от сервера " << reply->readAll();
}
