#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QObject>
#include "http.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    http Quer;
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    QObject * appWindow = engine.rootObjects().first();

    QObject::connect(appWindow,
                     SIGNAL(doQuer()),
                     &Quer,
                     SLOT(doQuer()));
    return app.exec();
}
