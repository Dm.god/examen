#ifndef HTTP_H
#define HTTP_H


#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QEventLoop>
#include <QStringList>
#include <QThread>

class http : public QObject
{
    Q_OBJECT
public:
    explicit http(QObject *parent = nullptr);
    QNetworkAccessManager * na_manager= new QNetworkAccessManager(this);
    QNetworkReply * reply;
    QNetworkRequest request;
signals:

public slots:
    void doQuer();
};

#endif // HTTP_H
